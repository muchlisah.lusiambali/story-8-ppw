from django import forms


class MatkulForm(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class': 'form-control'
    }
    nama = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Mata Kuliah ', max_length=100, required=True)
    dosen = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Nama Dosen ', max_length=100, required=True)
    jumlahsks = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Jumlah SKS ', max_length=100,
                                required=True)
    deskripsi = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Deskripsi Singkat ', max_length=300,
                                required=True)
    semestertahun = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Semester / Tahun ', max_length=100,
                                    required=True)
    ruangan = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Ruang Kelas ', max_length=100,
                              required=True)



"""
from django.forms import ModelForm
from .models import Schedule

class ScheduleForm(ModelForm):
    class Meta:
        model = Schedule
        fields = '__all__'


"""