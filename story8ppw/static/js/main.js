
!(function($) {
  "use strict";

  // Smooth scroll for the navigation menu and links with .scrollto classes
  $(document).on('click', '.nav-menu a, .mobile-nav a, .scrollto', function(e) {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      e.preventDefault();
      var target = $(this.hash);
      if (target.length) {

        var scrollto = target.offset().top;

        if ($('#header').length) {
          scrollto -= $('#header').outerHeight() - 2

        }

        if ($(this).attr("href") == '#header') {
          scrollto = 0;
        }

        $('html, body').animate({
          scrollTop: scrollto
        }, 1500, 'easeInOutExpo');

        if ($(this).parents('.nav-menu, .mobile-nav').length) {
          $('.nav-menu .active, .mobile-nav .active').removeClass('active');
          $(this).closest('li').addClass('active');
        }

        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
          $('.mobile-nav-overly').fadeOut();
        }
        return false;
      }
    }
  });

  // Mobile Navigation
  if ($('.nav-menu').length) {
    var $mobile_nav = $('.nav-menu').clone().prop({
      class: 'mobile-nav d-lg-none'
    });
    $('body').append($mobile_nav);
    $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
    $('body').append('<div class="mobile-nav-overly"></div>');

    $(document).on('click', '.mobile-nav-toggle', function(e) {
      $('body').toggleClass('mobile-nav-active');
      $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
      $('.mobile-nav-overly').toggle();
    });

    $(document).on('click', '.mobile-nav .drop-down > a', function(e) {
      e.preventDefault();
      $(this).next().slideToggle(300);
      $(this).parent().toggleClass('active');
    });

    $(document).click(function(e) {
      var container = $(".mobile-nav, .mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
          $('.mobile-nav-overly').fadeOut();
        }
      }
    });
  } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
    $(".mobile-nav, .mobile-nav-toggle").hide();
  }

  // Navigation active state on scroll
  var nav_sections = $('section');
  var main_nav = $('.nav-menu, #mobile-nav');

  $(window).on('scroll', function() {
    var cur_pos = $(this).scrollTop() + 80;

    nav_sections.each(function() {
      var top = $(this).offset().top,
        bottom = top + $(this).outerHeight();

      if (cur_pos >= top && cur_pos <= bottom) {
        if (cur_pos <= bottom) {
          main_nav.find('li').removeClass('active');
        }
        main_nav.find('a[href="#' + $(this).attr('id') + '"]').parent('li').addClass('active');
      }
    });
  });

  // Toggle .header-scrolled class to #header when page is scrolled
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('#header').addClass('header-scrolled');
    } else {
      $('#header').removeClass('header-scrolled');
    }
  });

  if ($(window).scrollTop() > 100) {
    $('#header').addClass('header-scrolled');
  }

  // Back to top button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });

  $('.back-to-top').click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 1500, 'easeInOutExpo', function() {
      $(".nav-menu ul:first li:first").addClass('active');
    });

    return false;
  });

  // Porfolio isotope and filter
  $(window).on('load', function() {
    var portfolioIsotope = $('.portfolio-container').isotope({
      itemSelector: '.portfolio-item',
      layoutMode: 'fitRows'
    });

    $('#portfolio-flters li').on('click', function() {
      $("#portfolio-flters li").removeClass('filter-active');
      $(this).addClass('filter-active');

      portfolioIsotope.isotope({
        filter: $(this).data('filter')
      });
    });

    // Initiate venobox (lightbox feature used in portofilo)
    $(document).ready(function() {
      $('.venobox').venobox();
    });
  });

  // Testimonials carousel (uses the Owl Carousel library)
  $(".testimonials-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      },
      900: {
        items: 3
      }
    }
  });

  // Portfolio details carousel
  $(".portfolio-details-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
    items: 1
  });

  // Initi AOS
  AOS.init({
    duration: 800
  });

})(jQuery);

const body = document.querySelector('svg');
const grow = document.querySelector('#grow');
const end = document.querySelector('#end');
const ring = document.querySelector('#ring');

const colors = ['green', 'gold', 'red'];

let followRing = 170;
let followMouse = 0;

const mod = (n, d) => ((n % d) + d) % d

function braid (newCount) {
    const count = grow.children.length;
    if (newCount === count) return;

    if (newCount < count) {
        for(let i = newCount; i < count; i++) {
            const g = grow.children[i];
            if (grow.children[i]) grow.removeChild(g);
        }
    } else {
        for(let i = count; i < newCount; i++) {
            const g = document.createElementNS("http://www.w3.org/2000/svg", 'g');
            grow.append(g);
            if (mod(i, 2)) {
                g.outerHTML = `<g id="run11" mask="url(#mrun1)" transform="matrix(1 0 0 -1 ${10 + Math.floor(i / 2) * 20} 70)">
   <use xlink:href="#b1" class="thread" />
   <rect x="35" y="10" width="30" height="32" transform="matrix(1,0,0,-1,30,69)" class="shadow-upper" />
   <use xlink:href="#hrun1" transform="matrix(1,0,0,-1,-10,70)" class="light" />
  </g>`
            } else {
                g.outerHTML = `<g id="run12" mask="url(#mrun1)" transform="translate(${Math.floor(i / 2) * 20})">
   <use xlink:href="#b1" class="thread" />
   <rect x="35" y="-5" width="30" height="35" transform="translate(30 29)" class="shadow-lower" />
   <use xlink:href="#hrun2" class="light" />
  </g>`
            }
        }
    }

    end.setAttribute('transform', `translate(${newCount * 10 - 60})`);
    for (let [i, g] of [...end.children].entries()) {
        g.classList.toggle('active', (newCount + i + 1) % 2);

        for (let thread of g.querySelectorAll('.thread')) {
            const base = parseFloat(thread.dataset.color);

            colors.forEach(c => thread.classList.remove(c));
            thread.classList.add(colors[mod(base + Math.floor(newCount / 2), 3)]);
        } 
    }
}

const follow = (event) => {
    const mx = body.getScreenCTM().inverse();
    let x = new DOMPoint(event.clientX, event.clientY).matrixTransform(mx).x;
    
    x = Math.max(x, 105);
    x = Math.min(x, 585);
    
    ring.setAttribute('transform', `translate(${followRing + x - followMouse})`);
    
    braid(Math.floor((x - 105) / 10));
}

ring.addEventListener('mousedown', (event) => {
    const mx = body.getScreenCTM().inverse();
    followMouse = new DOMPoint(event.clientX, event.clientY).matrixTransform(mx).x;

    body.addEventListener('mousemove', follow);
});

body.addEventListener('mouseup', () => {
    body.removeEventListener('mousemove', follow);

    followRing = 110 + grow.children.length * 10;
    ring.setAttribute('transform', `translate(${followRing})`);
});

gsap.registerPlugin(MotionPathPlugin);

gsap.to(".plane", {
  duration: 20,
  repeat: 12,
  repeatDelay: 0,
  ease: "none",
  motionPath: {
    path: "#mask-path",
    align: "#mask-path",
    autoRotate: 25,
    alignOrigin: [0.5, 0.49]
  }
});

const tl = gsap.timeline();

tl.fromTo(
  "#mask-path",
  { drawSVG: "0%" },
  { drawSVG: "100%", duration: 20, delay: 0.12, ease: "none" }
);
