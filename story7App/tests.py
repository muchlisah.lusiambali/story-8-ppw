from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import reverse, resolve
from . import views
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options

# Create your tests here.

class UnitTestForStory7(TestCase):
    def test_url_landing_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_landing_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, "index.html")
    
    def test_func_page(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, views.index)

    #test for views 
    def test_setup(self):
        self.client = Client()

    
    def test_GET_index(self):
        self.index = reverse("story7App:index")
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story7/index.html')

"""
# Func Test
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
"""